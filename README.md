# Dataverse #

Dataverse is a user-friendly tool for creating conversational forms that can also be accessed as chatbots, which store their data in dynamic databases.


### How do I get set up? ###

TODO

### Who do I talk to? ###

* Repo admin
* Original author at http://mayel.space

## Support on Beerpay
Hey dude! Help me out for a couple of :beers:!

[![Beerpay](https://beerpay.io/mayel/dataverse/badge.svg?style=beer-square)](https://beerpay.io/mayel/dataverse)  [![Beerpay](https://beerpay.io/mayel/dataverse/make-wish.svg?style=flat-square)](https://beerpay.io/mayel/dataverse?focus=wish)
