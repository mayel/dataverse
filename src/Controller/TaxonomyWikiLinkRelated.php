<?php
namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use RedBeanPHP\R;

class TaxonomyWikiLinkRelated extends Taxonomy
{


    public function import()
    {
        
        $wrels = R::findAll( 'wikirelation' , ' ORDER BY id ASC ' );

        // print_r($wrels);

        foreach($wrels as $wrel){
            $tag1 = ($wrel->tag);

            $tag2 = $this->tags_by_meta("Code", "HAWB", $wrel->wikid, true);

            print_r($tag1);
            print_r($tag2);

            if (!$tag2 || !$tag2->id) {
                echo("ERROR: Could not find tag $wrel->wikid !");
            } else {
                $this->tag_related_link($tag1, $tag2);
            }

        }

        return 'ok';
    }

    
}
