<?php
namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;

class AdminDB extends App
{
    /**
    * @Route("/admin/data", name="admin_data")
    */
    public function data()
    {
        global $bv;

        $this->admin_auth();
        error_reporting(0);

        $bv->base_path = $this->conf->base_path;
        $bv->adminer_path = $this->conf->base_path.'vendor/vrana/adminer/';

        require_once($this->conf->base_path.'inc/data.php');

        $inc_path = $bv->adminer_path."editor/";
        set_include_path(get_include_path() . PATH_SEPARATOR . $inc_path);
        require_once($inc_path."index.php");

        exit();
    }

    /**
    * @Route("/admin/db", name="admin_db")
    */
    public function db()
    {
        global $bv;

        $this->admin_auth();

        $bv->base_path = $this->conf->base_path;
        $bv->adminer_path = $this->conf->base_path.'vendor/vrana/adminer/';

        require_once($this->conf->base_path.'inc/db.php');

        $inc_path = $bv->adminer_path."adminer/";
        set_include_path(get_include_path() . PATH_SEPARATOR . $inc_path);
        require_once($inc_path."index.php");

        exit();
    }
}
